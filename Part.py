from OpenGL.GL import *
from OpenGL.GLU import *

from Generator import *

from Part_Manip import Is_Move_Command, Get_Coords_From_G1, Move_Line, Get_Total_Number_Of_Layers, Get_Part_Size, Scale_Line_By_Value, Set_Part_Size
from Data import DataToMap

import math

class Part():
  def __init__(self, file_name = ""):
    self.lines = []
    self.file_name = file_name
    if file_name != "":
      self.LoadFromFile(file_name)

    self.changed = False

  def GenerateRectangularMatrix(self, data):
    nmap = DataToMap(data)
    self.lines = Generate_Rectangle_Matrix(nmap)

  def LoadFromFile(self, file_name):
    with open(file_name) as f:
      self.lines = f.readlines()

  def Save(self):
    self.SaveToFile(self.file_name)

  def SaveToFile(self, file_name):
    with open(file_name, 'w') as f:
      for line in self.lines:
        f.write(line + "\n")
  
  def SetScale(self, to):
    self.lines = Set_Part_Size(self.lines, to)  
    self.changed = True

  def CenterBetween(self, size):
    print "Not implemented"
    self.changed = True

  def ScaleByValue(self, value):
     i = 0
     for line in self.lines:
        self.lines[i] = Scale_Line_By_Value(line, value)
        i += 1
     self.changed = True

  def Move(self, x, y, z):
    i = 0
    for line in self.lines:
      self.lines[i] = Move_Line(line, x, y, z)
      i += 1
    self.changed = True
        
  def draw(self):
    # Get total number of layers
    total = Get_Total_Number_Of_Layers(self.lines) / 2
    
    glPushMatrix() 
    glRotate(90, 1, 0, 0)

    glBegin(GL_LINE_LOOP)
    
    current_z = 0.0
    layer = 0
    color = 0

    for line in self.lines:
      if Is_Move_Command(line) == False:
        continue

      pos = Get_Coords_From_G1(line)  

      x = pos[0]
      y = pos[1]
      z = pos[2]
    
      if z != None:
        current_z = z
        layer += 1

      if x == None or y == None:
        continue
      color = ((1.0 * layer) / (1.0 * total) * 0.5) + 0.5
      glColor3f(color, color, color)
      glVertex3f(x, y, float(current_z))

    glEnd()
    glPopMatrix()
