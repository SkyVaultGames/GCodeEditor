import os, sys
from os import walk
import GCodeUtils as g

TOLERENCE = 0.5
DEFAULT_SCALE = 0.8
DEFAULT_SPEED = 50

def write_to_file(lines, path):
	with open(path, "w") as f:
		for line in lines:
			if line == '\n':
				continue
			if len(line) == 0:
				continue
			if line != None:
				f.write(line + '\n')

def scale_lines_xy_by(lines, scale, speed = DEFAULT_SPEED):
	i = 0
	for line in lines:
		lines[i] = g.scale_xy(line, scale, speed)
		i += 1
	return lines

def scale_xy_by(path, scale, speed = DEFAULT_SPEED):
	lines = g.read_all_lines(path)
	i = 0
	for line in lines:
		lines[i] = g.scale_xy(line, scale, speed)
		i += 1
	write_to_file(lines, path)

def scale_lines_z_by(lines, scale, speed = DEFAULT_SPEED):
	i = 0
	for line in lines:
		lines[i] = g.scale_z(line, scale, speed)
		i += 1
	return lines

def scale_z_by(path, scale, speed = DEFAULT_SPEED):
	lines = g.read_all_lines(path)
	i = 0
	for line in lines:
		lines[i] = g.scale_z(line, scale, speed)
		i += 1

	write_to_file(lines, path)

def handle_arguments():
	alen = len(sys.argv)
	if alen <= 3:
		print("Error:: not enough arguments!")
		print('''
arguments
	part
	axis - xy or z or xyz
	by - number or + or -
''')
		return
	elif alen == 4:
		part = sys.argv[1]
		axis = sys.argv[2]
		scale= sys.argv[3]
	
		if not os.path.isfile(part):
			print("Error:: cannot find part: ", part)
			return

		if axis == 'xy':
			if scale == '+':
				scale_xy_by(part, 1 + (1 - DEFAULT_SCALE))
			elif scale == '-':
				scale_xy_by(part, DEFAULT_SCALE)
			else:
				num = float(scale)
				scale_xy_by(part, num)
		if axis == 'z':
			if scale == '+':
				scale_z_by(part, 1 + (1 - DEFAULT_SCALE))
			elif scale == '-':
				scale_z_by(part, DEFAULT_SCALE)
			else:
				num = float(scale)
				scale_z_by(part, num)
		if axis == 'xyz':
			if scale == '+':
				scale_xy_by(part, 1 + (1 - DEFAULT_SCALE))
				scale_z_by(part, 1 + (1 - DEFAULT_SCALE))
			elif scale == '-':
				scale_xy_by(part, DEFAULT_SCALE)
				scale_z_by(part, DEFAULT_SCALE)
			else:
				num = float(scale)
				scale_xy_by(part, num)
				scale_z_by(part, num)

if __name__ == '__main__':
	handle_arguments()
