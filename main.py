import sys, math, os, glob, time, threading, csv
from PyQt4 import QtGui as q
from PyQt4 import QtCore
from PyQt4.QtOpenGL import *
from PyQt4.QtCore import QSize, QTimer, QTime
from PyQt4.QtGui import QWidget, QMainWindow, QToolBar, QTabWidget
from OpenGL.GL import *
from OpenGL.GLU import *
from borderlayout import BorderLayout
from Part import Part
from Data import CreateData, DATA_POINTS
from Ini_Parser import parse_ini_file as Parse_Ini
from PartGeneratorPanel import PartGeneratorPanel
from CodeView import CodeView
from DataViewer import DataViewer
from Camera3D import Camera3D

from Data import *
from Math3D import *

'''
Linux CNC
'''
import linuxcnc as cnc


SIZE = (800, 600)
PiOver2 = 3.14159 / 2.0

INI_PATH = glob.glob(os.path.expanduser("~/linuxcnc/configs/2gbp2/2gbp3.ini"))
INI_DATA = Parse_Ini(INI_PATH[0])

WIDTH = float(INI_DATA["[AXIS_0]"]["MAX_LIMIT"])
HEIGHT = float(INI_DATA["[AXIS_1]"]["MAX_LIMIT"])

class GLPartView(QGLWidget):
  def __init__(self, parent):
    super(GLPartView, self).__init__()
    
    self.board_width = float(INI_DATA["[AXIS_0]"]["MAX_LIMIT"])
    self.board_height = float(INI_DATA["[AXIS_1]"]["MAX_LIMIT"])

    
    self.main_parent = parent
    self.aspect = float(SIZE[0]) / float(SIZE[1])

    self.camera = Camera3D((-self.board_width / 2, -self.board_height / 2, -max(self.board_width, self.board_height) - 50))

    """
    Set up timing code for the part
    """
    self.updateTimer = QTimer()
    self.updateTimer.setSingleShot(False) 
    self.updateTimer.timeout.connect(self.update)
    self.updateTimer.start(1000 / 60)
    self.timer = 0.0
    self.installEventFilter(self)
  
    self.dt = 0
    self.getMills = lambda: int(round(time.time() * 1000))
    self.last = self.getMills() * 1.0

  def eventFilter(self, source, event):
    if event.type() == QtCore.QEvent.KeyPress:
      self.camera.handleKeyPress(event.key())
      return True
    elif event.type() == QtCore.QEvent.KeyRelease:
      self.camera.handleKeyRelease(event.key())
      return True
    else:
      return QGLWidget.eventFilter(self, source, event)
  def resizeEvent(self, event):
    pass

  def update(self):
    # Handle timing code
    now = self.getMills() * 1.0
    self.dt = (now - self.last) / 1000
    self.last = now

    self.main_parent.left_side_bar.update()
    self.main_parent.code_view.update()

    # TODO: Find a better way of requesting focus!
    #self.setFocus()
    self.timer += 0.0016
    self.camera.update(self.dt)
    self.repaint() # Re draw the screen

  def mousePressEvent(self, event):
    self.setFocus()

  def paintGL(self):
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glClearColor(0.2, 0.2, 0.2, 1)

    # adjust viewport
    size = self.size()
    aspect = (1.0 * size.width()) / (1.0 * size.height())
    glViewport(0, 0, size.width(), size.height())

    # Construct the view matrix
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    pers = M4.Perspective(45.0, aspect, 0.001, 10000.0)
    glLoadMatrixf(pers.getGLArray())
    #gluPerspective(45, aspect, 0.001, 1000.0)
    
    self.camera.projectViewMatrix()

    if self.main_parent.part != None:
      self.main_parent.part.draw()

    glPushMatrix()
    glRotate(90, 1, 0, 0)

    # Drawing the board
    width = self.board_width
    height = self.board_height
    glBegin(GL_LINE_LOOP)
    glColor4f(1, 0, 0, 1)
    glVertex3f(0, 0, 0)
    glColor4f(0, 1, 0, 1)
    glVertex3f(0, height, 0)
    glColor4f(0, 0, 1, 1)
    glVertex3f(width, height, 0)
    glColor4f(1, 0, 1, 1)
    glVertex3f(width, 0, 0)   
    glColor4f(1, 1, 1, 1)
    glEnd()
    
    rad = width / 2
    glBegin(GL_LINES)
    glColor3f(0.4, 0.4, 0.4)
    for i in range(0, 360):
      drad = i * (3.14159 / 180)
      glVertex2f(rad + math.cos(drad) * rad,rad + math.sin(drad) * rad)
    glColor3f(1, 1, 1)
    glEnd()
  
    # Drawing the axis lines
    glLineWidth(7.0)
    length = 20
    # Z
    glBegin(GL_LINES)
    glColor4f(0, 0, 1, 1)
    glVertex3f(0, 0, 0 + 0.3)
    glVertex3f(0, 0, length + 0.3)
    glColor4f(1, 1, 1, 1)
    glEnd()
  
    # Y
    glBegin(GL_LINES)
    glColor4f(0, 1, 0, 1)
    glVertex3f(0, 0, 0 + 0.3)
    glVertex3f(0, length, 0 + 0.3)
    glColor4f(1, 1, 1, 1)
    glEnd()

    # X
    glBegin(GL_LINES)
    glColor4f(1, 0, 0, 1)
    glVertex3f(0, 0, 0 + 0.3)
    glVertex3f(length, 0, 0 + 0.3)
    glColor4f(1, 1, 1, 1)
    glEnd()

    glLineWidth(0.5)
    glPopMatrix()

class MyApp(q.QWidget):
  def __init__(self):
    super(MyApp, self).__init__()
    self.createUI()
    self.part = Part("../gc/test.ngc")
    self.createLayout()

    self.data = []
    # TEMP
    #self.saveDataSheet()
    self.loadDataSheet()

  def saveDataSheet(self):
    print "save data sheet not imple"
  
  def loadDataSheet(self):
    print "loadDataSheet not implemented with new data"

  def createLayout(self):
    self.layout = BorderLayout()
    self.tabs = QTabWidget()

    # Toolbar
    self.main_toolbar = QToolBar()
    self.layout.addWidget(self.main_toolbar, BorderLayout.North)

    self.left_side_bar = PartGeneratorPanel(self, max(WIDTH, HEIGHT))

    # Set the generation parameters
    self.glView = GLPartView(self)
    self.code_view = CodeView(self)
    self.data_view = DataViewer(self)

    self.tabs.addTab(self.glView, "Part View")
    self.tabs.addTab(self.code_view, "GCode")
    self.tabs.addTab(self.data_view, "Data")

    self.layout.addWidget(self.tabs, BorderLayout.Center)
    self.layout.addWidget(self.left_side_bar, BorderLayout.West)

    self.setLayout(self.layout)
  
  def createUI(self):
    self.setGeometry(100, 100, 800, 800)
    self.show()

  def initLinuxCNC(self):
    try:
      s = cnc.stat()            # Create a connection to the status channel
      s.poll()                  # Get the current value

      c = cnc.command()
    except cnc.error, detail:
      print "linux cnc error:: ", detail
      raw_input()
      sys.exit(1)  
  
    ''' 
    SPAM INFORMATION TO THE SCREEN
    '''
    '''
    for x in dir(s):
      if not x.startswith('_'):
        print x, getattr(s, x)
    '''
  
    self.left_side_bar.giveCNCCommand(c)
  

if __name__ == '__main__':
  app = q.QApplication(sys.argv)
  myapp = MyApp()
  myapp.initLinuxCNC()
  sys.exit(app.exec_())
