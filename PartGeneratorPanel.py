from PyQt4.QtGui import *
from PyQt4.QtCore import *

from Data import CreateData, DATA_POINTS, DATA_MAPPER

import linuxcnc

'''
Part Type
  Cuboidal
  Circle 
  
Parameters
  Increase_each_layer: bool
  Shrink_each_layer: bool
  Extension_size: bool
  Extrusion_rate: float
  Line_Spacing: float
  Part_size
'''
GENERATOR_TYPES = [
  "Rectangular Matrix",
  "Circle",
  "Zamboni"
]

class HorizontalToolbar(QToolBar):
  def __init__(self):
    super(HorizontalToolbar, self).__init__()
    self.setOrientation(1)

class VerticalToolbar(QToolBar):
  def __init__(self):
    super(VerticalToolbar, self).__init__()
    self.setOrientation(2)

class PartGeneratorPanel(QWidget):
  def __init__(self, parent, board_size):
    super(PartGeneratorPanel, self).__init__()

    self.layout = QFormLayout()
    self.cnc = None

    self.parent = parent
    self.part = parent.part
    self.last_size = 0
    self.max_size = board_size
    self.speed = 1
    self.constructUI()

    self.data = CreateData()
  
    self.setLayout(self.layout)
    
  
  def constructUI(self):
    pglbl = QLabel("Part Generation")
    pglbl.setAlignment(Qt.AlignCenter)
    
    #self.addWidget(pglbl)
    
    self.type_selector = HorizontalToolbar()
    
    # Construct the generator combo box
    self.type_combo_box = QComboBox()
    for t in GENERATOR_TYPES:
      self.type_combo_box.addItem(t)
    self.layout.addRow(QLabel("Part Type: "), self.type_combo_box)

    # Part size
    self.part_size_slider = QSlider(1) # 1 is horizontal
    self.part_size_slider.setMinimum(2)
    self.part_size_slider.setMaximum(self.max_size)
    self.part_size_slider.setTickPosition(16)
    self.part_size_slider.valueChanged.connect(self.sizeChanged)
    self.layout.addRow(QLabel("Part Size: "), self.part_size_slider)

    # Movement buttons
    self.movement_toolbar = QHBoxLayout()
    self.layout.addRow(self.movement_toolbar)
    part_move_down = QToolButton()
    part_move_down.setArrowType(2)
    part_move_down.pressed.connect(lambda: self.part.Move(0, -self.speed, 0))

    part_move_up = QToolButton()
    part_move_up.setArrowType(1)
    part_move_up.pressed.connect(lambda: self.part.Move(0, self.speed, 0))

    part_move_left = QToolButton()
    part_move_left.setArrowType(3)
    part_move_left.pressed.connect(lambda: self.part.Move(-self.speed, 0, 0))

    part_move_right = QToolButton()
    part_move_right.setArrowType(4)
    part_move_right.pressed.connect(lambda: self.part.Move(self.speed, 0, 0))

    self.movement_toolbar.addWidget(part_move_left)
    self.movement_toolbar.addWidget(part_move_down)
    self.movement_toolbar.addWidget(part_move_up)
    self.movement_toolbar.addWidget(part_move_right)

    # Move by speed
    self.move_speed_slider = QSlider(1)
    self.move_speed_slider.setMinimum(0.1)
    self.move_speed_slider.setMaximum(10)
    self.move_speed_slider.setTickPosition(1)
    self.move_speed_slider.valueChanged.connect(self.setMovePartSpeed)
    self.layout.addRow(QLabel("Move Distance"), self.move_speed_slider)

    # Centering part
    self.part_center_button = QPushButton("Center Part")
    self.part_center_button.clicked.connect(self.centerPart) 
    self.layout.addRow(self.part_center_button)

    # Setting up the value form
    self.values = []
    for i in range(0, len(DATA_POINTS)):
      self.values.append(0)

    for name, value in DATA_POINTS:
      index = DATA_MAPPER[name]
      tb = QLineEdit()
      tb.insert(str(value))
      tb.setValidator(QDoubleValidator())
      tb.data = (name, value)
      self.values[index] = tb
      self.layout.addRow(QLabel(name), tb)
  
    # Generate button setup
    self.generate_button = QPushButton("Generate Part")
    self.generate_button.clicked.connect(self.generatePart)
    self.layout.addRow(self.generate_button)

    # Handing the machine
    self.layout.addRow(QLabel("MACHINE COMMANDS"))
  
    self.home_machine_btn = QPushButton("Home Machine")
    self.home_machine_btn.clicked.connect(self.homeMachine)
    self.layout.addRow(self.home_machine_btn)

    self.machine_jogger = QHBoxLayout()
    
    # Add the buttons
    # def moveMachine(self, speed, axis):
    speed = 20

    left  = QPushButton("<")
    left.pressed.connect(lambda: self.moveMachine(speed, 0))
    left.released.connect(lambda: self.stopMachineMovement(0))

    right = QPushButton(">")
    right.pressed.connect(lambda: self.moveMachine(-speed, 0))
    right.released.connect(lambda: self.stopMachineMovement(0))

    up    = QPushButton("^") 
    up.pressed.connect(lambda: self.moveMachine(-speed, 1))
    up.released.connect(lambda: self.stopMachineMovement(1))

    down  = QPushButton("v")
    down.pressed.connect(lambda: self.moveMachine(speed, 1))
    down.released.connect(lambda: self.stopMachineMovement(1))

    self.machine_jogger.addWidget(left)
    self.machine_jogger.addWidget(up)
    self.machine_jogger.addWidget(down)
    self.machine_jogger.addWidget(right)

    self.layout.addRow(self.machine_jogger)


  def update(self):
    pass

  def giveCNCCommand(self, command):
    self.cnc = command

  def stopMachineMovement(self, axis):
      self.cnc.jog(linuxcnc.JOG_STOP, axis)

  def moveMachine(self, speed, axis):
      self.cnc.jog(linuxcnc.JOG_CONTINUOUS, axis, speed)

  def homeMachine(self):
    print "Homing machine"

    if self.cnc != None:
      for i in range(0, 3):
        self.cnc.home(i)

  def setMovePartSpeed(self):
    self.speed = self.move_speed_slider.value()
  
  def centerPart(self):
    self.part.CenterBetween(None)

  def unit(self, val):
    if val > 0:
      return 1
    elif val == 0:
      return 0
    else:
      return -1

  def sizeChanged(self):
    self.part.SetScale(self.part_size_slider.value())

  def setInputValue(self, name, valu):
    index = DATA_MAPPER[name]
    self.values[index].setText(valu)

  def generatePart(self): 
    which = self.type_combo_box.currentText() 
    if which == "Rectangular Matrix": 
      print "generating a new part..."
      # JUMP
      if self.parent:
        data = CreateData()

        for point in DATA_POINTS:
          name = point[0]
          index= DATA_MAPPER[name] 
          text = self.values[index].text()
          data[index] = (name, float(text))  

        self.parent.data_view.populateTable(data)

        self.part.GenerateRectangularMatrix(data)

    """
    Save the data to a file
    """
    self.parent.part.Save()
    self.parent.data_view.exportDataTable()

  def ValueChanged(self, value):
    print(value)
  
