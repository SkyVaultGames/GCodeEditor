# TODO LIST

## Data
  1. make it so that if you click on one of the rows of data, it will 
     populate the side panel with that data.
  2. create a button that clears the side panel to the default values
  3. serialize the table to a csv file
  4. load that serialized csv file into the table
  5. create a simple 3d maths library for matrices maths
  6. use said library to implement a batter camera

## Stretch goals
  1. Move to the programmable pipeline
  2. Allow for mouse to part interactions
  3. Interface with linux cnc, to bypass using Axis

# DONE
  1. lets move to a pure data implementation, not a class based on.
  2. Get the generate part button linked to a callback.
  3. make it so that the generate button creates a new part with the new parameters.
