from PyQt4.QtGui import QToolBar, QTextEdit, QTextCursor, QTableWidget, QTableWidgetItem, QHeaderView
from PyQt4 import QtCore

from Data import CreateData, DATA_POINTS, DATA_MAPPER

import csv

DATA_FILE_NAME = "data_export.csv"

class DataViewer(QToolBar):
  def __init__(self, parent):
    super(DataViewer, self).__init__()
    self.parent = parent

    self.table = QTableWidget()
    self.table.setColumnCount(len(DATA_POINTS))
    self.table.itemSelectionChanged.connect(self.retreaveData)

    header = self.table.horizontalHeader()
    for point in DATA_POINTS:
      name = point[0]
      valu = point[1]
      index= DATA_MAPPER[name]
      header.setResizeMode(index, QHeaderView.Stretch)
      self.table.setHorizontalHeaderItem(index, QTableWidgetItem(name))
    self.addWidget(self.table)
    self.num_data_rows = 0
    self.importDataTable()

  def importDataTable(self):
    """
    Reading the csv file
    """
    with open(DATA_FILE_NAME, 'rb') as csvfile:
      reader = csv.reader(csvfile, delimiter=',', quotechar='|')
      for row in reader:
        data_points = []
        for i in range(0, len(row)):
          point = row[i]
          name = DATA_POINTS[i][0]
          data_points.append((name, point))
        self.populateTable(data_points)
    
  def exportDataTable(self):
    """
    Exports the table data to a csv
    """
    with open(DATA_FILE_NAME, 'wb') as csvfile:
      writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
      for i in range(0, self.table.rowCount()):
        row = [self.table.item(i, DATA_MAPPER["Speed"]).text(),
        self.table.item(i, DATA_MAPPER["Layers"]).text(),
        self.table.item(i, DATA_MAPPER["Density"]).text(),
        self.table.item(i, DATA_MAPPER["Layer_Height"]).text(),
        self.table.item(i, DATA_MAPPER["Size"]).text(),
        self.table.item(i, DATA_MAPPER["Extrusion_Rate"]).text(),
        self.table.item(i, DATA_MAPPER["Extension_Size"]).text()]
        
        writer.writerow(row)

  def populateTable(self, data):
    row_pos = self.table.rowCount()
    self.table.insertRow(0)
    
    for point in data:
      name = point[0]
      valu = point[1]
      index= DATA_MAPPER[name]
      item = QTableWidgetItem(str(valu))
      self.table.setItem(0, index, item)

    self.table.resizeRowsToContents()
    self.num_data_rows += 1

  def retreaveData(self):
    index = self.table.selectedIndexes()[0].row()

    gen = self.parent.left_side_bar

    gen.setInputValue("Speed",self.table.item(index, DATA_MAPPER["Speed"]).text())
    gen.setInputValue("Layers",self.table.item(index, DATA_MAPPER["Layers"]).text())
    gen.setInputValue("Density",self.table.item(index, DATA_MAPPER["Density"]).text())
    gen.setInputValue("Layer_Height",self.table.item(index, DATA_MAPPER["Layer_Height"]).text())
    gen.setInputValue("Size",self.table.item(index, DATA_MAPPER["Size"]).text())
    gen.setInputValue("Extrusion_Rate",self.table.item(index, DATA_MAPPER["Extrusion_Rate"]).text())
    gen.setInputValue("Extension_Size",self.table.item(index, DATA_MAPPER["Extension_Size"]).text())

