def begin(lines):
	lines.append("%")

def end(lines):
	begin(lines)

def read_all_lines(path):
	lines = []
	with open(path) as f:
		lines = f.readlines()
	return lines	

def get_x_y(line):
	x = 0
	y = 0
	if line.startswith("G1") == False:
		return None, x, y
	gline = line[3:]
	if gline.startswith("X"):
		xstr = gline.partition(" ")[0]
		gline = gline[len(xstr) + 1:]
		x = round(float(xstr[1:]), 4)
	else:
		return None, 0, 0
	if gline.startswith("Y"):
		ystr = gline.partition(" ")[0]
		y = round(float(ystr[1:]), 4)
	else:
		return None, 0, 0
	return True, x, y

def get_z(line):
	z = 0
	if line.startswith("G1") == False:
		return None, z
	gline = line[3:]
	if gline.startswith("Z"):
		xstr = gline.partition(" ")[0]
		gline = gline[len(xstr) + 1:]
		z = round(float(xstr[1:]), 4)
		return True, z
	else:
		return None, z

def get_axis(line, axis_name):
	axis = 0
	if line.startswith("G1") == False:
		return None, axis
	gline = line[3:]
	if gline.startswith(axis_name):
		xstr = gline.partition(" ")[0]
		gline = gline[len(xstr) + 1:]
		axis = round(float(xstr[1:]), 4)
		return True, axis
	else:
		return None, axis
	
	
def find_smallest_coord(lines):
	cx = 9999
	cy = 9999

	for line in lines:
		res, x, y = get_x_y(line)
		if res:
			if x < cx:
				cx = x
			if y < cy:
				cy = y
	return cx, cy

def find_largest_coord(lines):
	cx = 0
	cy = 0
	for line in lines:
		res, x, y = get_x_y(line)
		if res:
			if x > cx:
				cx = x
			if y > cy:
				cy = y
	return cx, cy

def find_center(lines):
	scx, scy = find_smallest_coord(lines)
	lcx, lcy = find_largest_coord(lines)
	return (scx + lcx) / 2, (scy + lcy) / 2

def g1_create(x, y, speed = 50):
	return "G1 X" + str(x) + " Y" + str(y) + " F" + str(speed)

def gl_create_z(z, speed):
	return "G1 Z" + str(z) + " F" + str(speed)

def gl_create_axis(axis_name, value ,speed):
	return "G1 " + axis_name + str(value) + " F" + str(speed)

def	move(line, dx, dy, speed):
	if line.startswith("G1") == False:
		return line
	res, x, y = get_x_y(line)
	if res:
		return g1_create(x + dx, y + dy, speed)
	else:
		return line

def	scale_xy(line, by, speed):
	if line.startswith("G1") == False:
		return line
	res, x, y = get_x_y(line)
	if res:
		return g1_create(x * by, y * by, speed)
	else:
		return line

def scale_z(line, by, speed):
	res, z = get_z(line)
	if res:
		return gl_create_z(z * by, speed)
	else:
		return line

def set_axis_speed(line, axis_name, speed):
	res, axis = get_axis(line, axis_name)
	if res:
		return gl_create_axis(axis_name, axis, speed)
	else:
		return line



