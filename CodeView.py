from PyQt4.QtGui import QToolBar, QTextEdit, QTextCursor

class CodeView(QToolBar):
  def __init__(self, parent):
    super(CodeView, self).__init__()
    self.parent = parent
    
    self.textview = QTextEdit()
    self.textview.setReadOnly(True)
    if self.parent.part:
      part = self.parent.part
      lines = part.lines
      line_number = 1
      for line in lines:
        self.textview.insertHtml(line + "<br>")
        line_number += 1

    self.addWidget(self.textview)

  def update(self):
    part = self.parent.part
