DEFAULT_SPEED = 25

def extract_next_axis(line):
	astr = line.partition(" ")[0]
	line = line[len(astr) + 1:]
	v = round(float(astr[1:]), 4)
	return v, line

def Is_Move_Command(line):
  return line.startswith("G1")

def Get_Coords_From_G1(line):
  assert(Is_Move_Command(line))
  g1_line = line[3:]

  x = None
  y = None
  z = None
  
  if g1_line.startswith("X"):
    x, g1_line = extract_next_axis(g1_line)
  if g1_line.startswith("Y"):
    y, g1_line = extract_next_axis(g1_line)
  if g1_line.startswith("Z"):
    z, g1_line = extract_next_axis(g1_line)

  return (x, y, z)

def Scale_Line_By_Value(line, value, speed = DEFAULT_SPEED):
	nline = "G1 "
	if line.startswith("G1") == False:
		return line

	g1_line = line[3:]

	if g1_line.startswith("X"):
		v, g1_line = extract_next_axis(g1_line)
		nline += "X" + str((v * value)) + " "
	else:
		return line

	if g1_line.startswith("Y"):
		v, g1_line = extract_next_axis(g1_line)
		nline += "Y" + str((v * value)) + " "
	else:
		return nline + "F" + str(speed)

	if g1_line.startswith("Z"):
		v, g1_line = extract_next_axis(g1_line)
		nline += "Z" + str(v) + " "
	else:
		return nline + "F" + str(speed)

	return nline + "F" + str(speed)

def Set_Part_Size(lines, to, speed = DEFAULT_SPEED):
  size = Get_Part_Size(lines)
  i = 0   
  for line in lines:

    nline = "G1 "
    if line.startswith("G1") == False:
      lines[i] = line

    g1_line = line[3:]
  
    if g1_line.startswith("X"):
      v, g1_line = extract_next_axis(g1_line)
      v = ((v / size) * to)
      nline += "X" + str(v) + " "
    else:
      lines[i] = line
      i += 1
      continue
  
    if g1_line.startswith("Y"):
      v, g1_line = extract_next_axis(g1_line)
      v = ((v / size) * to)
      nline += "Y" + str(v) + " "
    else:
      lines[i] = nline + "F" + str(speed)
      i += 1
      continue

    if g1_line.startswith("Z"):
      v, g1_line = extract_next_axis(g1_line)
      v = v / size
      nline += "Z" + str(v * to) + " "
    else:
      lines[i] = nline + "F" + str(speed)
      i += 1
      continue

    lines[i] = nline + "F" + str(speed)
    

    i += 1
  return lines

def Get_Part_Size(lines):
  max_size = 0
  for line in lines:
    if Is_Move_Command(line):
      x, y, z = Get_Coords_From_G1(line)

      larger = max(x, y)
      if larger > max_size:
        max_size = larger

  return max_size

def Get_Total_Number_Of_Layers(lines):
  number = 0
  for line in lines:
    if Is_Move_Command(line):
      x, y, z = Get_Coords_From_G1(line)
      if z != None:
        number += 1
  return number

def Move_Line(line, x, y, z, speed = DEFAULT_SPEED):
	nline = "G1 "
	if line.startswith("G1") == False:
		return line

	g1_line = line[3:]

	if g1_line.startswith("X"):
		v, g1_line = extract_next_axis(g1_line)
		nline += "X" + str(v + x) + " "
	else:
		return line

	if g1_line.startswith("Y"):
		v, g1_line = extract_next_axis(g1_line)
		nline += "Y" + str(v + y) + " "
	else:
		return nline + "F" + str(speed)

	if g1_line.startswith("Z"):
		v, g1_line = extract_next_axis(g1_line)
		nline += "Z" + str(v + z) + " "
	else:
		return nline + "F" + str(speed)

	return nline + "F" + str(speed)
