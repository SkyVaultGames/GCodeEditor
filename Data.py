DATA_POINTS = [
    ("Speed"          , 150),
    ("Layers"         , 32),
    ("Density"        , 0.85),
    ("Layer_Height"   , 0.090),
    ("Size"           , 16),
    ("Extrusion_Rate" , 0.00019),
    ("Extension_Size" , 5)
]

DATA_MAPPER = {
    "Speed"          :0,
    "Layers"         :1,
    "Density"        :2,
    "Layer_Height"   :3,
    "Size"           :4,
    "Extrusion_Rate" :5,
    "Extension_Size" :6
}

def DataToMap(data):
    nmap = {}
    for point in data:
      nmap[point[0]] = point[1]
    return nmap

def CreateData():
  return [
      ("Speed"          , 150),
      ("Layers"         , 32),
      ("Density"        , 0.85),
      ("Layer_Height"   , 0.090),
      ("Size"           , 16),
      ("Extrusion_Rate" , 0.00019),
      ("Extension_Size" , 5)
  ]
   
