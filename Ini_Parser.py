import re

def parse_ini_file(path):
	print("parsing config: " + path)

	lines = []
	cfg_map = {}
	
	current_base = ""
	with open(path) as f:
		lines = f.readlines()

	for line in lines:
		if line.startswith("#"):
			continue
		if line.startswith("["):
			current_base = line[:len(line)-1] # remove the carrage return
			cfg_map[current_base] = {}
		else:
			tokens = line.split("=")
			if len(tokens) == 2:
				var = tokens[0]
				val = tokens[1]
				# strip out unwanted charactors
				var = re.sub('[\n ]', '', var)
				val = re.sub('[\n ]', '', val)
	
				cfg_map[current_base][var] = val
				
	return cfg_map
