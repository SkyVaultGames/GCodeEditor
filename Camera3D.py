from OpenGL.GL import *
from OpenGL.GLU import *

from Math3D import *

import math

KEYS = {
 'w': 87,
 'a': 65,
 's': 83,
 'd': 68,
 'x': 88,
 'z': 90,
 '0': 48,
 'space': 32,
 'lshift': 16777248,
 'up': 16777235,
 'left': 16777234,
 'down': 16777237,
 'right': 16777236
}

class Camera3D():
  def __init__(self, pos):
      self.pos = pos
      self.start_pos = pos
      self.vx = 0
      self.vy = 0
      self.vz = 0

      self.position = V3(-pos[0],pos[1] * 4, -pos[2] / 2)
      self.front = V3(0, 0, 1);
      self.up = V3(0, 1, 0)

      self.rotation_x = 0

      self.speed = 50
      self.timer = 0.0

      self.yaw = -90.0 
      self.pitch = 90.0 

      self.keys = {
        KEYS['w']: False,
        KEYS['a']: False,
        KEYS['s']: False,
        KEYS['d']: False,
        KEYS['x']: False,
        KEYS['z']: False,
        KEYS['0']: False,
        KEYS['space']: False,
        KEYS['lshift']: False,
        KEYS['up']: False,
        KEYS['left']: False,
        KEYS['down']: False,
        KEYS['right']: False,
      }
  
  def projectViewMatrix(self):
    '''
    glTranslatef(self.pos[0], self.pos[1], self.pos[2])
    glRotatef(self.rotation_x, 1, 0, 0)
    '''

    # Test
    radius = 10.0
    camx = math.sin(self.timer) * radius
    camz = math.cos(self.timer) * radius

    view = M4.LookAt(self.position, self.position + self.front, self.up)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glLoadMatrixf(view.getGLArray())

  def handleKeyPress(self, key): 
    self.keys[key] = True

  def handleKeyRelease(self, key):
    self.keys[key] = False 

  def update(self, dt):
    x, y, z = self.pos
    self.timer += dt

    if self.keys[KEYS['left']]:
      self.yaw -= dt * 100
    if self.keys[KEYS['right']]:
      self.yaw += dt * 100

    if self.keys[KEYS['up']]:
      self.pitch += dt * 100
    if self.keys[KEYS['down']]:
      self.pitch -= dt * 100

    if self.keys[KEYS['0']]:
      self.pitch = 90.0
      self.yaw = -90.0

    # Gimble lock
    if (self.pitch > 89.0):
      self.pitch = 89.0

    if (self.pitch < -89.0):
      self.pitch = -89.0

    # Calculate the camera front vector
    front = V3(0, 0, 0)
    front.x = math.cos(self.pitch * DEGTORAD) * math.cos(self.yaw * DEGTORAD)
    front.y = math.sin(self.pitch * DEGTORAD)
    front.z = math.cos(self.pitch * DEGTORAD) * math.sin(self.yaw * DEGTORAD)
    self.front = V3.Normalize(front)
    
    move_speed = 5.0

    if self.keys[KEYS['w']]:
      self.position += V3.Normalize(V3.Cross(self.front, V3(1, 0, 0))) * move_speed 
    if self.keys[KEYS['s']]:
      self.position -= V3.Normalize(V3.Cross(self.front, V3(1, 0, 0))) * move_speed 

    if self.keys[KEYS['space']]: 
      self.position -= self.front * move_speed 
    if self.keys[KEYS['lshift']]: 
      self.position += self.front * move_speed 
    if self.keys[KEYS['a']]: 
      self.position += V3.Normalize(V3.Cross(self.front, self.up)) * move_speed
    if self.keys[KEYS['d']]:
      self.position -= V3.Normalize(V3.Cross(self.front, self.up)) * move_speed

    if self.keys[KEYS['space']]: pass
    if self.keys[KEYS['lshift']]: pass
